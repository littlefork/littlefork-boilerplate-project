# A scaffold for a simple Littlefork project

## Structure

- `node_modules/.bin/littlefork` :: The CLI for littlefork.
- `queries` :: Store your queries in this directory.
- `configs` :: Store your configurations in this directory.

## TODO

- Add the REST interface to the dependencies and expose a server.
- Expose prepared calls of the Littlefork command.
